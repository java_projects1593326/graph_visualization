import java.io.*;
import java.util.*;

class Device{

    //Initialize variables.
    private String deviceId;
    private String connectedDate;
    private String routerConnection;
    private String householdID;
    private String regionCode;
    private String productCategory;
    private String productType;
    private String productName;
    private boolean canSend;
    private boolean canReceive;

    
    //Device constructor.
    public Device(String deviceId, String connectedDate,  
                    String householdID, String regionCode, String productCategory, 
                    String productType, String productName, String routerConnection, boolean canSend, boolean canReceive){
        
        this.deviceId = deviceId;
        this.connectedDate = connectedDate;
        this.routerConnection = routerConnection;
        this.householdID = householdID;
        this.regionCode = regionCode;
        this.productCategory = productCategory;
        this.productType = productType;
        this.productName = productName;
        this.canSend = canSend;
        this.canReceive = canReceive;

    }

    //Prints deivce object information
    public void print(){

        System.out.println(deviceId + " " + connectedDate + " " + routerConnection + " " + householdID + " "+ regionCode + " " +
                            productCategory + " " + productType + " " + productName + " " + routerConnection + " " + canSend + " " + canReceive);
    }

    //Gets attributes of specific devices.
    public String getName(){
        return this.productName;
    }

    public String getProductType(){
        return this.productType;
    }

    public String getDeviceId(){
        return this.deviceId;
    }

    public String getProductCategory(){
        return this.productCategory;
    }

    public String getRouterConnection(){
        return this.routerConnection;
    }
}