import java.io.*;
import java.util.*;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;


class GraphMaker{
    //Initializes variables.
    private LinkedList<Device> devicesList = new LinkedList<Device>();
    private ArrayList<Device> routersList = new ArrayList<Device>();
    private ArrayList<Device> unlinkedDevices = new ArrayList<Device>();
    private Graph graph = new SingleGraph("Encost Devices");
    
    //Graph maker constructor.
    public GraphMaker(){
    }

    //Gets device objects from the linked list.
    public LinkedList getDevices(){
        return this.devicesList;
    }

    //Prints the device objects to console.
    public void printDevices(){
        for(Device d : devicesList){
            System.out.println(d.getName());
        }
    }

    //Adds device to deviceList or other lists based on device information.
    public void addDevice(Device d){
        //If product type is router than add to device list and router list.
        if(d.getProductType().contains("Router")){
            devicesList.add(d);
            routersList.add(d);
        }
        //Else call check routers.
        else{
            checkRouters(d);
        }

    }

    //Removes device from linked list.
    public void removeDevice(Device d){
        devicesList.remove(d);
    }

    //Removes all devices from linked list.
    public void clear(){
        for(Device d : devicesList){
            devicesList.remove(d);
        }
    }

    //Displays graph using graph stream library.
    public void displayGraph(){
        //Gets css file used to format graph that is displayed.
        String cssFilePath = new Scanner(GraphMaker.class.getResourceAsStream("graph.css"), "UTF-8").useDelimiter("\\A").next();
        graph.addAttribute("ui.stylesheet", cssFilePath);
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

        //Go through each item in the device list and create a node.
        for(Device d : devicesList){
            Node node = graph.addNode(d.getDeviceId());
            //Call add attributes function.
            addAttributes(node, d);

            //Go through each item in router list.
            for(Device r : routersList){
                String routerId = r.getDeviceId();
                String deviceId = d.getDeviceId();

                //If device is connected to router add edge between them.
                if(routerId.equals(d.getRouterConnection())){
                    graph.addEdge(routerId + deviceId, routerId, deviceId, true);
                }


            }
        }
        //Display graph.
        graph.display();
    }

    //Adds attributed based on device information.
    public void addAttributes(Node node, Device d){
        String category = d.getProductCategory();

        if(category.equals("Encost Wifi Routers")){
            node.setAttribute("Router", d); 
            node.setAttribute("ui.class", "router");     
        }
        else if(category.equals("Encost Hubs/Controllers")){
            node.setAttribute("HubController", d);
            node.setAttribute("ui.class", "hubController"); 
        }
        else if(category.equals("Encost Smart Lighting")){
            node.setAttribute("Lighting", d);
            node.setAttribute("ui.class", "lighting"); 
        }
        else if(category.equals("Encost Smart Appliances")){
            node.setAttribute("Lighting", d);
            node.setAttribute("ui.class", "appliances"); 
        }
        else if(category.equals("Encost Smart Whiteware")){
            node.setAttribute("Lighting", d);
            node.setAttribute("ui.class", "whiteware"); 
        }


    }

    //Checks if device passed in is connected to a router in the device list.
    private void checkRouters(Device b){
            int counter = devicesList.size();

            //If device is connected to router add to device list. Else add to unlinked devices.
            for(int i = 0; i < counter; i++){
                if(devicesList.get(i).getDeviceId().contains(b.getRouterConnection())){
                    devicesList.add(b);
                }
                else{
                    unlinkedDevices.add(b);
                }
        }
    }

}