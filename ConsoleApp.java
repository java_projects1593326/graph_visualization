import java.io.*;
import java.util.*;

public class ConsoleApp{

    //Initailization of variables.
    private static String userType;
    private static String userInput;
   
    private static boolean datasetLoaded = false;
    private static boolean userSelected = false;

    public static final String RED = "\033[0;31m";
    public static final String RESET = "\033[0m";

    private static Scanner myObj = new Scanner(System.in);
    private static ArrayList<Device> deviceList = new ArrayList<Device>();
    private static ArrayList<Users> userList = new ArrayList<Users>();
    private static GraphMaker graphData = new GraphMaker();

    //Initailizes counter variables (No longer required).
    private static int encostWifiRouters;
    private static int router;
    private static int encostRouter360;
    private static int encostRouterPlus;

    private static int extender;
    private static int encostWifiRangeExtender1;
    private static int encostWifiRangeExtender2;

    private static int encostHubsControllers;
    private static int hubController;
    private static int encostSmartHub;
    private static int encostSmartHub2;
    private static int encostSmartHubMini;

    private static int encostSmartLighting;
    private static int lightBulb;
    private static int encostSmartBulbB22White;
    private static int encostSmartBulbB22Multi;
    private static int encostSmartBulbE26White;
    private static int encostSmartBulbE26Multi;
    private static int stripLighting;
    private static int encostStripLightingWhite;
    private static int encostStripLightingMulti;
    private static int otherLighting;
    private static int encostNoveltyLightGiraffe;
    private static int encostNoveltyLightLion;
    private static int encostNoveltyLightBear;

    //Main where functions are called.
    public static void main(String[] args){
        clearConsole();

        fileParser();

        userSelect();

    }


    //User selection prompt.
    public static void userSelect(){
        
        try{
        //Creates loop so user select prompt stays till loop is broken by valid input.
        while(userSelected == false){
            String success;

            if(datasetLoaded == true){
                success = "Dataset Successfully Loaded";
            }
            else{
                success = "";
            }
            //Prompt shown to user.
            String outPut = "Welcome to the Encost Smart Graph Project \n\n" +
                            "Loading Dataset... \n" + success + "\n\n" +
                            "Enter user (1/2) \n" +
                            "\t (1) Community User \n" +
                            "\t (2) Encost User \n" +
                            "\t (x) Exit \n\n" +
                            ">> ";

            System.out.print(outPut);

            userInput = myObj.nextLine();
            //If user selects 1, proceed to feature select prompt.
            if(userInput.equals("1")){
                userType = "Community User";
                
                featureSelect();
                break;

            }
            //If user selects 2, proceed to user login prompt.
            else if(userInput.equals("2")){
                
                userLogin();
                break;
            }
            //If user selects x, than exit program.
            else if(userInput.equals("x")){
                break;
            }
            //All other input is invalid so show user select prompt still.
            else{
               
                clearConsole();
                System.out.println("Invalid Input");
                System.out.println();
                
            }

        }
        }
        catch(Exception e){
            System.err.print(e);
        }


    }


    //User login prompt.
    public static void userLogin(){
        clearConsole();
        String inputUsername = "";
        String inputPassword = "";

        Console console = System.console();
        try{
            while(userSelected == false){
                    //Ask user for username.
                    String userNameOutput = "Enter Username: ";
                    System.out.print(userNameOutput);
                    //Save input
                    inputUsername = myObj.nextLine();
                    //Ask user for password.
                    String passwordOutput = "Enter Password: ";
                    System.out.print(passwordOutput);
                    char [] pwd = console.readPassword();
                    //Save input
                    inputPassword = new String(pwd);


                    String usernameAndPassword = inputUsername + inputPassword;
                    
                    //Go through the users in user class and compare input to saved encost users.
                    for(Users u : userList){
                        //If correct than procceed to feature select prompt.
                        if(u.Login().equals(usernameAndPassword)){
                            userType = "Encost User";
                            featureSelect();
                            
                        }
                    }
                    if(userType == "Encost User"){
                        break;
                    }
                    //All other input is invalid, go back to user select show error message.
                    else{
                        clearConsole();
                        System.out.println(RED + "Invalid Username or Password" + RESET + "\n");
                        userSelect();
                        break;
                    }
            }
        }
        catch(Exception e){
            System.err.println(e);
        }
    }

    //Gives feature select prompt.
    public static void featureSelect(){
        
        clearConsole();
        
        try{
        while(userSelected == false){
            //If community user show this prompt.
            if(userType == "Community User"){
                String communityConsoleOutput = "User type: " + userType + 
                                        "\n \n" + 
                                        "ESGP Features: \n" +
                                        "\t (1) Graph Visualization \n" +
                                        "\t (x) Exit \n" +
                                        "Select Option >> ";
                System.out.print(communityConsoleOutput);
                userInput = myObj.nextLine();
                if(userInput.equals("1")){
                    clearConsole();
                    graphData.displayGraph();
                    
                    featureSelect();
                    break;
                }
                else if(userInput.equals("x")){
                    clearConsole();
                    userSelect();
                    userType = "";
                    break;
                }
                else{
                    clearConsole();
                    System.out.println("Invalid Input");
                }
            }
            //If user is encost user show this prompt.
            else if(userType == "Encost User"){
                String encostConsoleOutput = "User type: " + userType + 
                                        "\n \n" + 
                                        "ESGP Features: \n" +
                                        "\t (1) Graph Visualization \n" +
                                        "\t (2) Load Custom Dataset \n" +
                                        "\t (3) View Summary Statistics \n" +
                                        "\t (x) Exit \n" +
                                        "Select Option >> ";
                System.out.print(encostConsoleOutput);
                userInput = myObj.nextLine();
                if(userInput.equals("1")){
                    graphData.displayGraph();
                    featureSelect();
                    break;
                }
                else if(userInput.equals("2")){
                    break;
                }
                else if(userInput.equals("3")){
                    deviceDistribution();
                    break;
                }
                else if(userInput.equals("x")){
                    clearConsole();
                    userSelect();
                    
                    userType = "";
                    break;
                }
                else{
                    clearConsole();
                    System.out.println("Invalid Input");
                }

            }
        }
        }
        catch(Exception e){
            System.err.println(e);
        }
    }

    


    //Passes in Encost Smart Home Devices dataset from default location.
    public static void fileParser(){
        try{
        File dataFile = new File("EncostSmartHomesDataset.txt");
        File userFile = new File("users.txt");
        Scanner reader = new Scanner(dataFile);
        

        String data = reader.nextLine();

        //Reads through the Encost Smart Home dataset.
        while(reader.hasNextLine()){
            data = reader.nextLine();
            
            //Splits data read accordingly.
            String[] deviceLine = data.split(",");
            
            String[] householdIDSplit = deviceLine[4].split("-");
            String regionCode = householdIDSplit[0];

            String category = categorizer(deviceLine[3]);

            boolean canSend;
            boolean canReceive;
            //Translates string read to boolean variable needed.
            if(deviceLine [6] == "Yes"){
                canSend = true;
            }
            else{
                canSend = false;
            }
            if(deviceLine[7] == "Yes"){
                canReceive = true;
            }
            else{
                canReceive = false;
            }
            //Makes device object from the data obtained from the readline.
            Device deviceInput = new Device(deviceLine[0], deviceLine[1], deviceLine[4], 
                                    regionCode, category, deviceLine[3], deviceLine[2], deviceLine[5],
                                    canSend, canReceive);
            //Adds object to list.
            deviceList.add(deviceInput);
            
        }
        reader.close();

        Scanner reader2 = new Scanner(userFile);

        data = reader2.nextLine();

        //Reads in users file.
        while(reader2.hasNextLine()){
            data = reader2.nextLine();

            String[] userLine = data.split(",");

            //Creates user object and saves user data.
            Users user = new Users(userLine[0], userLine[1]);
            userList.add(user);
        }
        reader2.close();
        datasetLoaded = true;

        //Adds devices to graphData class.
        for(Device d : deviceList){
            graphData.addDevice(d);
        }
        }
        catch(Exception e){
            System.out.println(e);

        }
    }

    //Returns a category for the device based on the device type.
    public static String categorizer(String type){

        String result = "";

        if(type.contains("Router")  || type.contains("Extender")){
            result = "Encost Wifi Routers";
            return result;
        }
        else if(type.contains("Hub/ Controller") ){
            result = "Encost Hubs/Controllers";
            return result;
        }
        else if(type.contains("Light bulb") || type.contains("Strip Lighting") || type.contains("Other Lighting ")){
            result = "Encost Smart Lighting";
            return result;
        }
        else if(type.contains("Kettle") || type.contains("Toaster") || type.contains("Coffee Maker")){
            result = "Encost Smart Appliances";
            return result;
        }
        else if(type.contains("Washing Machine/Dryer") || type.contains("Refrigerator/Freezer") || type.contains("Dishwasher")){
            result = "Encost Smart Whiteware";
            return result;
        }
        else{
            return null;
        }

    }

    //Calculated Device distribution (No longer required.)
    public static void deviceDistribution(){
    
        
        try{
        for(Device d: deviceList){
            if(d.getName().equals("Encost Router 360")){
                encostRouter360 ++;
                router ++;
                encostWifiRouters ++;
            }
            else if(d.getName().equals("Encost Router Plus")){
                encostRouterPlus ++;
                router ++;
                encostWifiRouters ++;
            }
            else if(d.getName().equals("Encost Wifi Range Extender 1.0")){
                encostWifiRangeExtender1 ++;
                extender ++;
                encostWifiRouters ++;
            }
            else if(d.getName().equals("Encost Wifi Range Extender 2.0")){
                encostWifiRangeExtender2 ++;
                extender ++;
                encostWifiRouters ++;
            }
            else if(d.getName().equals("Encost Smart Hub")){
                encostSmartHub ++;
                hubController ++;
                encostHubsControllers ++;
            }
            else if(d.getName().equals("Encost Smart Hub 2.0")){
                encostSmartHub2 ++;
                hubController ++;
                encostHubsControllers ++;
            }
            else if(d.getName().equals("Encost Smart Hub Mini")){
                encostSmartHubMini ++;
                hubController ++;
                encostHubsControllers ++;
            }
            else if(d.getName().equals("Encost Smart Hub Mini")){
                encostSmartHubMini ++;
                hubController ++;
                encostHubsControllers ++;
            }
            else if(d.getName().equals("Encost Smart Bulb B22 (white)")){
                encostSmartBulbB22White ++;
                lightBulb ++;
                encostSmartLighting ++;
            }
            
            else if(d.getName().equals("Encost Smart Bulb B22 (multi colour)")){
                encostSmartBulbB22Multi ++;
                lightBulb ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Smart Bulb E26 (white)")){
                encostSmartBulbE26White ++;
                lightBulb ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Smart Bulb E26 (multi colour)")){
                encostSmartBulbE26Multi ++;
                lightBulb ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Strip Lighting (white)")){
                encostStripLightingWhite ++;
                stripLighting ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Strip Lighting (multi colour)")){
                encostStripLightingMulti ++;
                stripLighting ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Novelty Light (giraffe)")){
                encostNoveltyLightGiraffe ++;
                otherLighting ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Novelty Light (giraffe)")){
                encostNoveltyLightGiraffe ++;
                otherLighting ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Novelty Light (lion)")){
                encostNoveltyLightLion ++;
                otherLighting ++;
                encostSmartLighting ++;
            }
            else if(d.getName().equals("Encost Novelty Light (bear)")){
                encostNoveltyLightBear ++;
                otherLighting ++;
                encostSmartLighting ++;
            }

        }
        clearConsole();
        while(userSelected == false){
        String outPut = "Device Distribution \n\n" +

                        "Encost Wifi Routers \t\t\t\t" + Integer.toString(encostWifiRouters) + "\n" + 
                        "\t Routers \t\t\t\t" + Integer.toString(router) + "\n" +
                        "\t\t Encost Router 360 \t\t\t" + Integer.toString(encostRouter360) + "\n" +
                        "\t\t Encost Router Plus \t\t\t" + Integer.toString(encostRouterPlus) + "\n" +
                        "\t Extender \t\t\t\t\t" + Integer.toString(extender) + "\n" +
                        "\t\t Encost Wifi Range Extender 1.0 \t\t" + Integer.toString(encostWifiRangeExtender1) + "\n" +
                        "\t\t Encost Wifi Range Extender 2.0 \t\t" + Integer.toString(encostWifiRangeExtender2) + "\n\n" +
                        "Encost Hubs/Controllers \t\t\t\t" + Integer.toString(encostHubsControllers) + "\n" +
                        "\t Hub/Controller \t\t\t\t" + Integer.toString(hubController) + "\n" +
                        "\t\t Encost Smart Hub \t\t" + Integer.toString(encostSmartHub) + "\n" +
                        "\t\t Encost Smart Hub 2.0 \t\t" + Integer.toString(encostSmartHub2) + "\n" +
                        "\t\t Encost Smart Hub Mini \t\t" + Integer.toString(encostSmartHubMini) + "\n\n" +
                        "Encost Smart Lighting \t\t\t\t" + Integer.toString(encostSmartLighting) + "\n" +
                        "\t Light Bulb \t\t\t\t" + Integer.toString(lightBulb) + "\n" +
                        "\t\t Encost Smart Bulb B22 (White)\t\t" + Integer.toString(encostSmartBulbB22White) + "\n" +
                        "\t\t Encost Smart Bulb B22 (Multi Colour)\t\t" + Integer.toString(encostSmartBulbB22Multi) + "\n" +
                        "\t\t Encost Smart Bulb E26 (White) \t\t" + Integer.toString(encostSmartBulbE26White) + "\n\n" +
                        "\t\t Encost Smart Bulb E26 (Multi Colour) \t\t" + Integer.toString(encostSmartBulbE26Multi) + "\n\n" +
                        "\t Strip Lighting\t\t\t\t" + Integer.toString(stripLighting) + "\n" +
                        "\t\t Encost Strip Lighting (White)\t\t" + Integer.toString(encostStripLightingWhite) + "\n" +
                        "\t\t Encost Strip Lighting (Multi Colour)\t\t" + Integer.toString(encostStripLightingMulti) + "\n" +
                        "\t Other Lighting\t\t\t\t" + Integer.toString(otherLighting) + "\n" +
                        "\t\t Encost Novelty Light (Giraffe)\t\t" + Integer.toString(encostNoveltyLightGiraffe) + "\n" +
                        "\t\t Encost Novelty Light (Lion)\t\t" + Integer.toString(encostNoveltyLightLion) + "\n" +
                        "\t\t Encost Novelty Light (Bear)\t\t" + Integer.toString(encostNoveltyLightBear) + "\n";

        System.out.print(outPut);
        System.out.print("Press x to exit >>> ");
        userInput = myObj.nextLine();
        if(userInput == "x"){
            featureSelect();
            break;
        }else{
            clearConsole();
            deviceDistribution();
        }

        }

        }
        catch(Exception e){
            System.err.println(e);
        }

    }

    //Clears Console.
    public static void clearConsole(){
        System.out.print("\033[H\033[2J");  

        System.out.flush();  
    }





}