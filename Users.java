class Users{

    //Initilizes variables.
    private String userName;
    private String password;

    //Users contructor.
    public Users(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    //Gets user name.
    public String getUsername(){
        return this.userName;
    }

    //Gets password.
    public String getPassword(){
        return this.password;
    }

    //Puts username and password together to be compared.
    public String Login(){
        return this.userName + this.password; 
    }
}